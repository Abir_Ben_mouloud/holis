# HOLIS 


# **README for Table Detection Project**
**Groupe:InsightSeekers**

## **Project Overview**

This project aims to automate the process of detecting and extracting tables from unstructured document formats, such as PDFs, by using machine learning and computer vision techniques. The project encompasses several stages, including separating native text PDFs from scanned PDFs, extracting native tables, converting PDFs to images, and training a Convolutional Neural Network (CNN) model for table detection.

## **Environment Setup**

- Python 3.x
- Libraries required: `PyMuPDF`, `tabula-py`, `pandas`, `pdf2image`, `fitz`, `cv2`, `numpy`, `tensorflow`

To install the necessary libraries, run:

```bash
pip install PyMuPDF tabula-py pandas pdf2image opencv-python numpy tensorflow
```

Java is also required for `tabula-py` to function properly.

## **Project Structure**

- `pdf_folder/`: Contains all PDF files to be processed.
- `pdf_folder/text_native/`: Contains native text PDFs after separation.
- `pdf_folder/scanned/`: Contains scanned PDFs after separation.
- `pdf_folder/extracted_tables_text_native/`: Contains extracted tables from native text PDFs in Excel format.
- `pdf_folder/scanned/sortie/`: Contains the output images from scanned PDFs.
- `pages/`: Contains the converted images from PDFs.
- `train_images/`: Contains images for training the CNN model.

## **Usage**

### **Separating PDFs into Native Text and Scanned**

The code distinguishes between native text PDFs and scanned PDFs, moving them into separate directories for further processing.

### **Native Table Extraction**

For PDFs with native text, `tabula-py` is used to extract tables and save them in an Excel format.

### **Conversion of PDF to Images**

Scanned PDFs are converted to images using `PyMuPDF` and `pdf2image`. This is a crucial step for preparing data for CNN training.

Après avoir convertir les pages du pdf en images .Ce code crée un modèle de réseau de neurones convolutionnel (CNN) utilisant des images comme données d'entrée et des étiquettes associées pour reconnaître les tableaux en jaune.L'idée serait d'entraîner ce modèle à reconnaître les régions où se trouvent les tableaux (en utilisant le dossier "SPARE_EXPLICATIONS" où les tableaux sont annotés en jaunes) et l'appliquer aux autres documents du dossier "SPARE_DOCUMENTS" . Après viendra l'étape où on découpera les régions dans lesquelles le modèle a prédit l'existence de tableaux , puis on appliquera une technique ocr (tesseract par exemple) pour extraire la table en texte.

### **Training CNN Model**

The CNN model is a simple architecture designed to detect tables within images. The training data consists of images with tables highlighted in yellow regions. The model is trained with data augmentation techniques to improve its ability to generalize and detect tables in various contexts.

### **Evaluating and Saving the Model**

After training, the model is evaluated on a separate test dataset to determine its accuracy. The final trained model is saved for future use.

## **Potential Improvements and Examples for Unfinished Steps**

While the full objectives of the project could not be completed within the given timeframe, here are some potential steps and examples that can be used to finish the project:

1. **Dataset Annotation:**
   - Use tools like `labelImg` or `CVAT` to annotate your images with bounding boxes around the tables.
   - Save the annotations in a format compatible with your model, such as COCO or VOC.

2. **Advanced CNN Training:**
   - Consider using a pre-trained model like ResNet or VGG as a starting point for transfer learning.
   - Fine-tune the model on your dataset with annotations.

3. **Detection and Extraction:**
   - Apply the trained model to new images to detect tables.
   - Use detected bounding boxes to crop the tables from the images.
   - For OCR (Optical Character Recognition), integrate Tesseract or a similar tool to convert the table images into editable text.

4. **Model Deployment:**
   - Deploy the model as a web service using frameworks like Flask or FastAPI.
   - Create a user interface for uploading documents and displaying extracted tables.

## **Conclusion**

This README provides a comprehensive guide to the current state of the project and outlines future steps for completion. The provided code snippets and instructions should serve as a solid foundation for further development and refinement of the table detection system.
